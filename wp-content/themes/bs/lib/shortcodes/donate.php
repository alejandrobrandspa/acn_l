<?php

function bs_donate_form_sc($atts) {
  ob_start();
  ?>

  <donate-form
    donation_type="<?php echo $atts['donation_type'] ?>"
    url="<?php echo get_template_directory_uri() ?>/lib/stripe.php"
    captcha_name="<?php echo $atts['captcha_name'] ?>"
    >
    </donate-form>

  <template id="change-amount-template">
    <ul class="change-amount">
      <li><a href="#" @click="changeAmount(10, $event)">€10</a></li>
      <li><a href="#" @click="changeAmount(30, $event)">€30</a></li>
      <li><a href="#" @click="changeAmount(50, $event)">€50</a></li>
      <li><a href="#" @click="changeAmount(100, $event)">€100</a></li>
      <li><a href="#" @click="changeAmount('', $event)"><?php echo getT('Other') ?></a></li>
    </ul>
  </template>

  <template id="donate-form-template">
    <div v-if="!success">
    <change-amount></change-amount>

    <form method="post">

    <div class="card-data">

    <div class="form-group">
      <div class="input-group col-sm-12" >
        <div class="input-group-addon" style="font-weight: 700; font-size: 30px">EUR €</div>
        <input
          type="text"
          class="form-control"
          style="font-size: 30px"
          v-model="amount"
          v-el:amount-input
          @keyup="cleanNumber('amount')"
          placeholder="<?php echo getT('Amount') ?>"
        >
      </div>
    </div>

      <div class="form-group">
          <input
            type="text"
            @keyup="[cleanNumber('stripe.number'), maxLength('stripe.number', 16)],showCard()"
            class="form-control form-control--outline"
            style="font-size: 30px"
            id="exampleInputAmount"
            v-model="stripe.number"
            placeholder="<?php echo getT('Credit Card Number') ?>"
          >
      </div>

      <div class="form-group col-sm-6 " style="float: none; margin: 0 auto 20px auto">
        <img v-bind:class="{'card-type--active': card.Visa}" class="card-type" src="<?php echo get_template_directory_uri() ?>/img/cards/Visa.png" alt="">
        <img v-bind:class="{'card-type--active': card.MasterCard}" class="card-type" src="<?php echo get_template_directory_uri() ?>/img/cards/MasterCard.png" alt="">
        <img v-bind:class="{'card-type--active': card.DinersClub}" class="card-type" src="<?php echo get_template_directory_uri() ?>/img/cards/DinersClub.png" alt="">
        <img v-bind:class="{'card-type--active': card.AmericanExpress}" class="card-type" src="<?php echo get_template_directory_uri() ?>/img/cards/AmericanExpress.png" alt="">
        <img v-bind:class="{'card-type--active': card.Discover}" class="card-type" src="<?php echo get_template_directory_uri() ?>/img/cards/Discover.png" alt="">
      </div>

      <div class="row row-centered" >
        <div class="form-group col-sm-3">
          <input
            type="text"
            @keyup="[cleanNumber('stripe.exp_month'), maxLength('stripe.exp_month', 2)]"
            class="form-control form-control--outline"
            style="text-align: center; font-size: 30px"
            placeholder="<?php echo getT('MM') ?>"
            v-model="stripe.exp_month"
          >
        </div>

        <div class="form-group col-sm-3">
            <input
              type="text"
              @keyup="[cleanNumber('stripe.exp_year'), maxLength('stripe.exp_year', 2)]"
              class="form-control form-control--outline"
              style="text-align: center; font-size: 30px"
              placeholder="<?php echo getT('YY') ?>"
              v-model="stripe.exp_year"
            >
        </div>

      <div class="form-group col-sm-3">
          <input
            type="text"
            @keyup="[cleanNumber('stripe.cvc'), maxLength('stripe.cvc', 4)]"
            class="form-control form-control--outline"
            style="text-align: center; font-size: 30px"
            v-model="stripe.cvc"
            placeholder="<?php echo getT('CVC') ?>"
          >
      </div>
      </div>

    </div>

    <div class="donate__contact-data" v-if="stripe.token">

      <h6><?php echo getT('PLEASE FILL YOUR INFORMATION') ?></h6>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group ">
            <input
              type="text"
              name="name"
              class="form-control"
              placeholder="<?php echo getT('Name') ?>"
              v-model="contact.name"
              >
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group">
            <input
              type="text"
              name="email"
              class="form-control"
              placeholder="<?php echo getT('Email') ?>"
              v-model="contact.email"
            >
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <select name="gender" class="form-control" v-model="contact.gender">
              <option value="" selected><?php echo getT('Gender') ?></option>
              <option value="male"><?php echo getT('Male') ?></option>
              <option value="female"><?php echo getT('Female') ?></option>
            </select>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <select class="form-control" name="country" v-model="contact.country">
              <?php if(getCountry() != "default"): ?>
                <option value="<?php echo getCountry() ?>" selected><?php echo getCountry() ?></option>
              <?php else: ?>
                <option value="" selected><?php echo getT("Country") ?></option>
              <?php endif; ?>

              <option value="" selected><?php echo getT('Country') ?></option>
                <?php foreach(getCountryList() as $country): ?>
                  <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                <?php endforeach; ?>
            </select>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <input
              type="text"
              name="mobile"
              class="form-control"
              placeholder="<?php echo getT('Mobile') ?>"
              v-model="contact.mobile"
            >
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <input
              type="text"
              name="address"
              class="form-control"
              placeholder="<?php echo getT('Address') ?>"
              v-model="contact.address"
            >
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <input
              type="text"
              name="city"
              class="form-control"
              placeholder="<?php echo getT('City') ?>"
              v-model="contact.city"
            >
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <input
              type="text"
              name="postal_code"
              class="form-control"
              placeholder="<?php echo getT('Postal Code') ?>"
              v-model="contact.postal_code"
            >
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">

        <div class="alert alert-danger" v-if="errors">
          <span v-if="errors.amount"><?php echo getT('Amount') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.number"><?php echo getT('Credit Card Number') . ' ' . getT('incorrect') ?>, </span>
          <span v-if="errors.exp_month"><?php echo getT('Month') . ' ' . getT('incorrect') ?>, </span>
          <span v-if="errors.exp_year"><?php echo getT('Year') . ' ' . getT('incorrect') ?>, </span>
          <span v-if="errors.cvc"><?php echo getT('CVC') . ' ' . getT('incorrect') ?></span>
          <span v-if="errors.captcha"><?php echo getT('Captcha') . ' ' . getT('required') ?></span>
          <span v-if="errors.stripe">{{errors.stripe}}</span>

          <span v-if="errors.name"><?php echo getT('Name') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.email"><?php echo getT('Email') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.gender"><?php echo getT('Gender') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.country"><?php echo getT('Country') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.mobile"><?php echo getT('Mobile') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.address"><?php echo getT('Address') . ' ' . getT('required') ?>, </span>
          <span v-if="errors.city"><?php echo getT('City') . ' ' . getT('required') ?>, </span>

        </div>
      </div>
    </div>

    <div id="{{ captcha_name }}" style="width: 304px; margin: 20px auto"></div>

    <a href="#" class="donate-heart-btn-base" style="width: 220px; height: 110px; margin: 0 auto" @click.prevent="onSubmit" v-if="stripe.token" :disabled="loading">
      <svg viewBox="610 1527 220 104" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <defs class="rect">
              <rect  id="path-1" x="0" y="0" width="218" height="80"></rect>
              <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="218" height="80" fill="white">
                  <use xlink:href="#path-1"></use>
              </mask>
          </defs>

          <g id="Group-Copy" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(611.000000, 1530.000000)">
              <g id="Group-7" class="line" transform="translate(0.000000, 20.000000)" stroke-width="8" stroke="#FFFFFF">
                  <g id="Button-/-Disabled-Copy">
                      <use id="Button-Background" mask="url(#mask-2)" xlink:href="#path-1"></use>
                  </g>
              </g>

              <path class="path heart" d="M129.477316,8.16485859 C128.795983,6.56270466 127.815754,5.12763516 126.612746,3.91534488 C125.402312,2.70491109 123.95796,1.72653899 122.363233,1.05077534 C120.761079,0.376868189 119.004836,0 117.155768,0 C115.314127,0 113.55974,0.464123385 111.95573,1.28283704 C110.361002,2.09598121 108.918506,3.26371564 107.706216,4.6560858 C107.373904,5.04223645 107.060156,5.45066503 106.763117,5.86837608 C106.466078,5.44880854 106.154187,5.04223645 105.821875,4.6560858 C104.611441,3.26371564 103.167089,2.09598121 101.572361,1.28283704 C99.9702074,0.464123385 98.2139646,0 96.364897,0 C94.5232554,0 92.761443,0.376868189 91.1648586,1.05077534 C89.5627047,1.72653899 88.1276352,2.70491109 86.9153449,3.91534488 C85.7049111,5.12763516 84.726539,6.56270466 84.0507753,8.16485859 C83.3768682,9.76144303 83,11.5232554 83,13.364897 C83,15.2139646 83.3768682,16.9702074 84.0507753,18.5723614 C84.726539,20.1670893 85.7049111,21.6114413 86.9153449,22.8218751 C88.1276352,24.0248829 105.16282,39.4207838 106.764974,40.1039734 C108.359702,39.4207838 125.402312,24.0248829 126.612746,22.8218751 C127.815754,21.6114413 128.795983,20.1670893 129.477316,18.5723614 C130.151223,16.9702074 130.528091,15.2139646 130.528091,13.364897 C130.528091,11.5232554 130.151223,9.76144303 129.477316,8.16485859 L129.477316,8.16485859 Z" id="Page-1" stroke="#FC3938" stroke-width="5"></path>
              <path class="path line-mask" d="M39.5,22 L108.760376,22" id="Line" stroke="#FFFFFF" stroke-width="4" stroke-linecap="square"></path>
          </g>
      </svg>
      <span style="font-size: 20px"><?php echo getT('Donate') ?></span>
    </a>

    <a href="#" class="donate-heart-btn-base" style="width: 220px; height: 110px; margin: 0 auto" @click.prevent="getToken" v-else :disabled="loading">
      <svg viewBox="610 1527 220 104" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <defs class="rect">
              <rect  id="path-1" x="0" y="0" width="218" height="80"></rect>
              <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="218" height="80" fill="white">
                  <use xlink:href="#path-1"></use>
              </mask>
          </defs>

          <g id="Group-Copy" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(611.000000, 1530.000000)">
              <g id="Group-7" class="line" transform="translate(0.000000, 20.000000)" stroke-width="8" stroke="#FFFFFF">
                  <g id="Button-/-Disabled-Copy">
                      <use id="Button-Background" mask="url(#mask-2)" xlink:href="#path-1"></use>
                  </g>
              </g>

              <path class="path heart" d="M129.477316,8.16485859 C128.795983,6.56270466 127.815754,5.12763516 126.612746,3.91534488 C125.402312,2.70491109 123.95796,1.72653899 122.363233,1.05077534 C120.761079,0.376868189 119.004836,0 117.155768,0 C115.314127,0 113.55974,0.464123385 111.95573,1.28283704 C110.361002,2.09598121 108.918506,3.26371564 107.706216,4.6560858 C107.373904,5.04223645 107.060156,5.45066503 106.763117,5.86837608 C106.466078,5.44880854 106.154187,5.04223645 105.821875,4.6560858 C104.611441,3.26371564 103.167089,2.09598121 101.572361,1.28283704 C99.9702074,0.464123385 98.2139646,0 96.364897,0 C94.5232554,0 92.761443,0.376868189 91.1648586,1.05077534 C89.5627047,1.72653899 88.1276352,2.70491109 86.9153449,3.91534488 C85.7049111,5.12763516 84.726539,6.56270466 84.0507753,8.16485859 C83.3768682,9.76144303 83,11.5232554 83,13.364897 C83,15.2139646 83.3768682,16.9702074 84.0507753,18.5723614 C84.726539,20.1670893 85.7049111,21.6114413 86.9153449,22.8218751 C88.1276352,24.0248829 105.16282,39.4207838 106.764974,40.1039734 C108.359702,39.4207838 125.402312,24.0248829 126.612746,22.8218751 C127.815754,21.6114413 128.795983,20.1670893 129.477316,18.5723614 C130.151223,16.9702074 130.528091,15.2139646 130.528091,13.364897 C130.528091,11.5232554 130.151223,9.76144303 129.477316,8.16485859 L129.477316,8.16485859 Z" id="Page-1" stroke="#FC3938" stroke-width="5"></path>
              <path class="path line-mask" d="M39.5,22 L108.760376,22" id="Line" stroke="#FFFFFF" stroke-width="4" stroke-linecap="square"></path>
          </g>
      </svg>
      <span style="font-size: 20px"><?php echo getT('Donate') ?></span>
    </a>

  </form>
  </div>

  <div class="alert alert-success" style="text-align: center" v-if="success">
    <h2><?php echo getT('Thanks for your support; your donation enable us to continue helping those in need !. For any information regarding your gift, you may contact us to info@acnmercy.org') ?></h2>
  </div>

  </template>
  <?php

  return ob_get_clean();
}

  add_shortcode( 'donate_form', 'bs_donate_form_sc' );
?>
