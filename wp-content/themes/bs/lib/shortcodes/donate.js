'use strict';
const _ = require('lodash');
const moment = require('moment');
const gaEvents = require('../ga_events')();
const gaEcommerce = require('../ga_ecommerce');
const validateStripe = require('../stripe/validation.js');
// 
Stripe.setPublishableKey('pk_test_kORhSnXY5TPJMXXY5Wwiugzy');

let componentData = {
  errors: null,
  success: false,
  loading: false,
  stripe: {
    number: '',
    exp_month: '',
    exp_year: '',
    cvc: '',
    token: ''
  },

  contact: {
    name: null,
    email: null,
    gender: null,
    country: null,
    mobile: null,
    address: null,
    city: null,
    postal_code: null,
    stripe_token: null
  },
  card: {
    Visa: false,
    MasterCard: false,
    DinersClub: false,
    AmericanExpress: false,
    Discover: false
  },

  captcha: null,
  created_at: moment().format(),
  amount: 30
};

module.exports = Vue.extend({
  template: "#donate-form-template",

  props: [
    'donation_type',
    'captcha_name',
    'url'
  ],

  data() {
    return $.extend(true, {}, componentData);
  },

  computed: {
    cardType() {
      let type = Stripe.card.cardType(this.stripe.number).replace(" ", "");
      return type;
    }
  },

  events: {
    'focus-amount': function () {
      this.amount = 1;
      this.$els.amountInput.focus();
    }
  },

  methods: {

    showCard() {
      Object.keys(this.card).map(key => {
        if(key === this.cardType) {
          return this.card[key] = true;
        } else {
          return this.card[key] = false;
        }
      });
    },

    cleanNumber(keypath) {
      let val = this.$get(keypath);
      this.$set(keypath, val.replace(/[^0-9]+/, ''));
    },

    maxLength(keypath, length) {
      let val = this.$get(keypath);
      this.$set(keypath, val.substring(0, length));
    },

    isRequired(keypath) {
      let error = {};
      let val = this.$get(keypath) ? this.$get(keypath) : '';

      if(val === "") {
         error[keypath] = true;
      } else {
        error[keypath] = false;
      }

      return error;
    },

    createToken() {
      let stripeData = {
        number: this.stripe.number,
        cvc: this.stripe.cvc,
        exp_month: this.stripe.exp_month,
        exp_year: this.stripe.exp_year
      };

      this.toggleLoading();

      Stripe.card.createToken(stripeData, this.handleToken);
    },

    handleToken(status, response) {
      this.toggleLoading();

      if(response.id) {
        this.stripe.token = response.id;
      }

      if(response.error) {
        this.errors = {stripe: response.error.message};
      }
    },

    contactValidations() {
      let fields = [
        'contact.name',
        'contact.email',
        'contact.gender',
        'contact.country',
        'contact.mobile',
        'contact.address',
        'contact.city'
      ];

      let errors = {};

      fields.forEach((key) => {
        errors = _.extend(errors, this.isRequired(key));
      });

      this.errors = errors;

    },

    showErrors() {
      let errorAmount = this.isRequired('amount');
      this.errors = _.extend(validateStripe(this.stripe).errors, errorAmount);
    },

    removeErrors() {
      this.errors = null;
    },

    toggleLoading() {
      this.loading = !this.loading;
    },

    cleanData() {
      this.stripe = _.extend(this.stripe, componentData.stripe);
      this.contact = _.extend(this.contact, componentData.contact);
      this.captcha = false;
      grecaptcha.reset( BS[this.captcha_name] );
    },

    getToken(e) {
      e.preventDefault();

      if(validateStripe(this.stripe).success) {
        this.removeErrors();
        this.createToken();
      } else {
        this.showErrors();
      }
    },

    onSubmit(e) {
      e.preventDefault();

      this.captcha = grecaptcha.getResponse(BS[this.captcha_name]);

      this.contactValidations();

      let data = _.extend(this.contact, {
        amount: this.amount,
        donation_type: this.donation_type,
        stripe_token: this.stripe.token,
        captcha: this.captcha
      });

      this.toggleLoading();

      $.ajax({
        url: this.url,
        type: 'POST',
        data: data,
        beforeSend: () => {
          this.removeErrors();
        }
      })
      .then(this.handleSubmitResponse);

    },

    handleSubmitResponse(res) {
      let response = {};

      try {
        response = JSON.parse(res);
      } catch (e) {
        this.removeErrors();
        console.log('donate response err: ', res);
      }

      this.toggleLoading();

      if(response.success) {
        this.removeErrors();
        this.success = true;

        let subdata = `?customer_id=${response.stripe.customer}&order_revenue=${this.amount}&order_id=${response.stripe.id}`;

        if(this.donation_type == 'monthly') {
          gaEvents.donateMonthly();
          gaEcommerce(response.stripe.id, null, this.amount);
          if(fbq) fbq('track', 'Purchase', {value: this.amount, currency: 'EUR'});

          window.location = `http://acnmercy.org/donate-monthly-thanks/${subdata}`;
        }

        if(this.donation_type == 'once') {
          gaEvents.donateUnique();
          gaEcommerce(response.stripe.id, null, this.amount);
          if(fbq) fbq('track', 'Purchase', {value: this.amount, currency: 'EUR'});
          window.location = `http://acnmercy.org/donate-once-thanks/${subdata}`;
        }

      } else if(response.errors) {
        this.errors = response.errors;
      }
    }
  }
});
