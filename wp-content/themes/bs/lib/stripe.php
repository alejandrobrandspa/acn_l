<?php
require 'vendor/autoload.php';
use Respect\Validation\Validator as v;
\Stripe\Stripe::setApiKey("sk_test_vq5s51SGycQ6dvCqC3H7JcCl");

require 'thanks_mail.php';

class donationData {

  var $post;

  //donation type
  var $donation_type;

  //stripe
  var $stripe_token;

  //charge
  var $amount;

  //customer
  var $name;
  var $gender;
  var $email;
  var $mobile;
  var $country;
  var $city;
  var $address;
  var $postal_code;
  var $created_at;

  function setAtt($name) {
    $value = '';

    if(isset($this->post[$name]) ) {
      $value = $this->post[$name];
    }

    $this->{$name} = $value;
  }

  function __construct($post) {

    $this->post = $post;

    $atts = array_keys(get_object_vars($this));

    //remove attribute post from been set again
    unset($atts[0]);

    //set each attribute from post array
    foreach ($atts as $key => $value) {
      $this->setAtt($value);
    }

    return $atts;
  }

}

function createToken() {
  $token = \Stripe\Token::create(array(
    "card" => array(
      "number" => "4242424242424242",
      "exp_month" => 5,
      "exp_year" => 2017,
      "cvc" => "314"
    )
  ));

  return $token;
}

function createCustomer($donationData) {
  $customer = \Stripe\Customer::create(array(
    "description" => "Customer for " . $donationData->donation_type,
    "email" => $donationData->email,
    "metadata" => array(
      'name' =>  $donationData->name,
      'mobile' =>  $donationData->mobile,
      'country' =>  $donationData->country,
      'city' =>  $donationData->city,
      'address' =>  $donationData->address,
      'postal_code' =>  $donationData->postal_code,
    ),
    "source" => $donationData->stripe_token
  ));

  return $customer;
}


function createPlan($donationData) {
  $name = "donation-" . $donationData->amount;
  $amountRaw = (int) $donationData->amount;
  $amount = $amountRaw . "00";

  try {
    $plan = \Stripe\Plan::create(array(
      "amount" => $amount,
      "interval" => "month",
      "id" => $name,
      "name" => $name,
      "currency" => "eur"
    ));
  } catch(Exception $e) {
    return $name;
  }

  return $plan;
}

function getPlan($amountRaw) {
  $id = null;

  switch ($amountRaw) {
    case '10':
      $id = "donation-10";
      break;
    case '20':
      $id = "donation-20";
    break;
    case '30':
      $id = 'donation-30';
    break;
    case '50':
      $id = "donation-50";
      break;
    case '100':
      $id = "donation-100";
    break;
    default:
      $id = null;
    break;
  }

  return $id;
}

function createSubscription($customerId, $planId) {

  $subscription = \Stripe\Subscription::create(array(
    "customer" => $customerId,
    "plan" => $planId
  ));

  return $subscription;
}

function createCharge($donationData, $customerId) {
  $amountRaw = (int) $donationData->amount;
  $amount = $amountRaw . "00";

  $charge = \Stripe\Charge::create(array(
    "amount" => $amount,
    "currency" => "eur",
    "customer" => $customerId
  ));

  return $charge;
}

function SubscribeMonthly($donationData) {

  try {
    $customer = createCustomer($donationData);
  } catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  }

  $planId = getPlan($donationData->amount);

  if($planId == null) {
    $planId = createPlan($donationData);
  }

  try {
    $subs = createSubscription($customer->id, $planId);
  } catch(Exception $e) {
    $planId = createPlan($donationData);
    $subs = createSubscription($customer->id, $planId);

    echo 'Message: ' .$e->getMessage();
  }

  return $subs;
}

function oneCharge($donationData) {

  try {
    $customer = createCustomer($donationData);
  } catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  }

  $charge = createCharge($donationData, $customer->id);

  return $charge;
}

$donationData = new donationData($_POST);

function verifyCaptcha($post) {
  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $secret = urlencode('6LdCPiITAAAAABYRzPbRaAb-eHmFi_GapYz-pvRU');
  $token = $post['captcha'];
  $response = urlencode($token);

  $verifyResponse = file_get_contents($url . '?secret=' . $secret. '&response=' . $response);
  $responseData = json_decode($verifyResponse);

  if($responseData->success) {
    return true;
  } else {
    return false;
  }
}



function isDonationDataValid($post) {
  $errors = array();

  $validateRequired = array( 'name', 'gender', 'email', 'mobile', 'country', 'city', 'address' );

  foreach ($validateRequired as $key => $val) {
    if( v::stringType()->notEmpty()->validate( $post[$val] ) ) {
      $errors[$val] = false;
    } else {
      $errors[$val] = true;
    }
  }

  $pass = array_filter($errors, function($err) {
    return $err == true;
  });

  if(empty($pass)) {
    return array("success" => true);
  } else {
    return array("success" => false, "errors" => $errors, "pass" => $pass);
  }

}

if(isDonationDataValid($_POST)['success']) {

  $isTokenValid = verifyCaptcha($_POST);

  if($isTokenValid) {

    if(isset($donationData->donation_type) && $donationData->donation_type == 'monthly') {

      $stripeResponse = SubscribeMonthly($donationData);
      sendThanksMail($donationData->email);
      echo( json_encode( array("success" => true, stripe =>  $stripeResponse) ) );
      return false;

    } elseif($donationData->donation_type == 'monthly') {

      echo(json_encode(array("monthly" => true, "success" => false)));
      return false;
    }

    if(isset($donationData->donation_type) && $donationData->donation_type == 'once') {

      $stripeResponse = oneCharge($donationData);
      sendThanksMail($donationData->email);
      echo( json_encode( array("success" => true, stripe =>  $stripeResponse) ) );
      return false;

    } elseif($donationData->donation_type == 'once') {

      echo(json_encode(array("once" => true, "success" => false)));
      return false;

    }

  } else {

    echo json_encode(array("success" =>  false, "errors" => array("captcha" => true)  ));
    return false;
  }

} else {
  echo json_encode(isDonationDataValid($_POST));
  return false;
}

?>
