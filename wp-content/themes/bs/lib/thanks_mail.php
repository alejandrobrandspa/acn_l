<?php
require 'vendor/autoload.php';

function sendThanksMail($to) {
  $sg = new \SendGrid('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');

  $body = json_decode('{
    "personalizations": [
      {
        "to": [
          {
            "email": "'. $to .'"
          }
        ],
        "subject": "Thank you for your support"
      }
    ],
    "from": {
      "email": "info@acn.com",
      "name": "ACN International"
    },
    "content": [
      {
        "type": "text/html",
        "value": "Hello, World!"
      }
    ],
    "template_id": "6b5354cb-60e9-4b47-b48c-787a6f336563"
  }');

  $response = $sg->client->mail()->send()->post($body);
  $response->statusCode();
  $response->body();
  $response->headers();
  return $response;
}


?>
