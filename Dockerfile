FROM php:5.6-fpm
VOLUME /var/www/html

COPY . /var/www/html

RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev && rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd mysqli

CMD ["php-fpm"]
